package com.mollberg.javablackjack.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class IntegerUtilsTest {

    @Test
    public void isInt_happyPath() {
        assertThat(IntegerUtils.isInt("123"), is(true));
    }

    @Test
    public void isInt_nonNumber() {
        assertThat(IntegerUtils.isInt("ABC"), is(false));
    }

    @Test
    public void isInt_longValue() {
        assertThat(IntegerUtils.isInt(String.valueOf(Long.MAX_VALUE)), is(false));
    }
}
