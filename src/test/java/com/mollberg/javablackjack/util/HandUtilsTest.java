package com.mollberg.javablackjack.util;

import com.mollberg.javablackjack.game.hand.holder.player.PlayerUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Set;

import static com.mollberg.javablackjack.game.constant.Card.*;
import static com.mollberg.javablackjack.game.constant.HandAction.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
public class HandUtilsTest {

    @Test
    public void getHandDisplayValue_aceComesBackWithSoftPrefix() {
        assertThat(HandUtils.getHandDisplayValue(Arrays.asList(ACE, FIVE)), is("soft 16"));
    }

    @Test
    public void getHandDisplayValue_hardAceComesBackWithoutSoft() {
        assertThat(HandUtils.getHandDisplayValue(Arrays.asList(ACE, FIVE, TEN)), is("16"));
    }

    @Test
    public void getHandDisplayValue_twoAces() {
        assertThat(HandUtils.getHandDisplayValue(Arrays.asList(ACE, ACE)), is("soft 12"));
    }

    @Test
    public void getHandDisplayValue_noAces() {
        assertThat(HandUtils.getHandDisplayValue(Arrays.asList(EIGHT, QUEEN)), is("18"));
    }


    @Test
    public void getPossibleActions() {
        assertThat(PlayerUtils.getPossibleActions(null, null), is(Set.of(DOUBLE, HIT, SPLIT, STAND)));
    }
}