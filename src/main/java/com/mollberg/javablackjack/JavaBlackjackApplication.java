package com.mollberg.javablackjack;

import com.mollberg.javablackjack.game.runner.GameRunner;
import com.mollberg.javablackjack.game.table.TableService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JavaBlackjackApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBlackjackApplication.class, args);
    }

    @Bean
    public GameRunner gameRunner(TableService tableService) {
        return new GameRunner(tableService);
    }
}
