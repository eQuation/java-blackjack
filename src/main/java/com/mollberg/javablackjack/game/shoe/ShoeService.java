package com.mollberg.javablackjack.game.shoe;

import com.mollberg.javablackjack.game.constant.Card;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static com.mollberg.javablackjack.game.constant.Card.*;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;
import static java.util.Arrays.asList;
import static java.util.Collections.shuffle;
import static java.util.stream.Collectors.toList;

@Service
public class ShoeService {
    private final int decks = 6;

    private final double ratio = 0.75;

    public Shoe newShoe() {
        return Shoe.builder()
                .cards(shuffleCards(newCards()))
                .cutCard(generateCutCard())
                .build();
    }

    private int generateCutCard() {
        return toIntExact(round(decks * 52 * ratio));
    }

    private List<Card> newCards() {
        return IntStream.range(0, decks)
                .mapToObj(__ -> newDeck())
                .flatMap(List::stream)
                .collect(toList());
    }

    private List<Card> newDeck() {
        return IntStream.range(0, 4)
                .mapToObj(__ -> new ArrayList<>(asList(
                        ACE,
                        KING,
                        QUEEN,
                        JACK,
                        TEN,
                        NINE,
                        EIGHT,
                        SEVEN,
                        SIX,
                        FIVE,
                        FOUR,
                        THREE,
                        TWO)))
                .flatMap(List::stream)
                .collect(toList());
    }

    private List<Card> shuffleCards(final List<Card> unshuffledCards) {
        final List<Card> shuffledCards = new ArrayList<>(unshuffledCards);
        shuffle(shuffledCards);
        return shuffledCards;
    }

    public boolean stillPlayable(final Shoe shoe) {
        return shoe.getNextCardIndex() < shoe.getCutCard();
    }

    public Card dealCard(final Shoe shoe) {
        final Card card = shoe.getCards().get(shoe.getNextCardIndex());
        shoe.setNextCardIndex(shoe.getNextCardIndex() + 1);
        return card;
    }
}
