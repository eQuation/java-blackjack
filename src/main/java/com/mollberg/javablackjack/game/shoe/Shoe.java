package com.mollberg.javablackjack.game.shoe;

import com.mollberg.javablackjack.game.constant.Card;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Shoe {

    private List<Card> cards;

    @Builder.Default
    private int nextCardIndex = 0;

    private int cutCard;
}
