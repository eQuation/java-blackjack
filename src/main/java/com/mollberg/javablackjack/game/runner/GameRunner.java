package com.mollberg.javablackjack.game.runner;

import com.mollberg.javablackjack.game.table.TableService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameRunner {
    public GameRunner(final TableService tableService) {
        tableService.initialize();
    }
}
