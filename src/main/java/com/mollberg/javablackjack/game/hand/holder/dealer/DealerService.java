package com.mollberg.javablackjack.game.hand.holder.dealer;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.util.HandUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.mollberg.javablackjack.game.constant.HandAction.HIT;
import static com.mollberg.javablackjack.game.constant.HandAction.STAND;
import static com.mollberg.javablackjack.util.HandUtils.*;

@Service
public class DealerService {
    public HandAction getAction(final List<Card> hand) {
        final int handValue = getHandValue(hand);

        if (handValue > 16) {
            return STAND;
        }

        if (isSoft(hand) && handValue > 7) {
            return STAND;
        }

        return HIT;
    }

    public boolean shouldPlay(final List<Hand> playerHands) {
        return playerHands.stream()
                .map(Hand::getCards)
                .anyMatch(HandUtils::notBusted);
    }
}
