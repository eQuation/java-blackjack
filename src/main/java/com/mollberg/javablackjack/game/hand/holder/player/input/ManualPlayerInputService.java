package com.mollberg.javablackjack.game.hand.holder.player.input;


import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.hand.holder.player.output.PlayerOutputService;
import com.mollberg.javablackjack.game.table.Table;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import static com.mollberg.javablackjack.game.constant.HandAction.STAND;
import static com.mollberg.javablackjack.game.constant.HandAction.allInputTexts;
import static com.mollberg.javablackjack.game.hand.holder.player.PlayerUtils.getPossibleActions;
import static com.mollberg.javablackjack.util.IntegerUtils.isInt;
import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Objects.isNull;

@Service
@Profile("manual")
public class ManualPlayerInputService implements PlayerInputService {

    private final Scanner scanner;

    private final PlayerOutputService playerOutputService;

    public ManualPlayerInputService(final PlayerOutputService playerOutputService) {
        this.scanner = new Scanner(System.in);
        this.playerOutputService = playerOutputService;
    }

    @Override
    public boolean continueGame() {
        return getYesNoInput("Continue the game?");
    }

    @Override
    public HandAction getAction(final Hand hand, final Player player, final Table table) {
        playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());

        final List<String> allInputs = allInputTexts();

        HandAction action = null;
        while (isNull(action)) {
            final Set<HandAction> possibleActions = getPossibleActions(player, hand);

            if (possibleActions.size() == 1) {
                action = STAND;
                continue;
            } else {
                playerOutputService.outputActions(possibleActions);
            }

            playerOutputService.output("Enter your action: ", false);
            final String input = getUserInput();

            if (!allInputs.contains(input)) {
                continue;
            }
            action = HandAction.fromInputText(input);
        }

        return action;
    }

    @Override
    public int getWager(final Player player) {
        int wager = -1;
        while (!validWager(wager, player)) {
            playerOutputService.outputMoney(player);
            playerOutputService.output("Enter your wager: ", false);
            final String input = getUserInput();

            if (!isInt(input)) {
                continue;
            }
            wager = parseInt(input);
        }

        return wager;
    }

    private String getUserInput() {
        return scanner.next();
    }

    private boolean getYesNoInput(final String displayText) {
        String input = null;
        while (!asList("Y", "y", "N", "n").contains(input)) {
            playerOutputService.output(format("%s (y/n): ", displayText), false);
            input = getUserInput();
        }

        return asList("Y", "y").contains(input);
    }

    private boolean validWager(final int wager, final Player player) {
        return wager > 0 && wager <= player.getMoney();
    }
}
