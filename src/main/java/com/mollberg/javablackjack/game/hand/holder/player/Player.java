package com.mollberg.javablackjack.game.hand.holder.player;

import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.Person;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Player extends Person {

    private List<Hand> hands;

    private int money;
}
