package com.mollberg.javablackjack.game.hand.holder.player.output;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.table.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.mollberg.javablackjack.util.HandUtils.getHandDisplayValue;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

@Service
@Profile("manual")
public class TerminalPlayerOutputService implements PlayerOutputService {

    private final PrintStream printStream;

    @Autowired
    public TerminalPlayerOutputService() {
        this.printStream = System.out;
    }

    @Override
    public void output(final String output) {
        output(output, true);
    }

    @Override
    public void output(final String output, final boolean newLine) {
        if (newLine) {
            printStream.println(output);
        } else {
            printStream.print(output);
        }
    }

    @Override
    public void clearOutput() {
        printStream.print("\033[2J");
    }

    @Override
    public void outputMoney(final Player player) {
        output(format("Player has %d to play with.", player.getMoney()));
    }

    @Override
    public void outputTableInfo(final Table table, final boolean hideDownCard) {
        outputSeparator();
        outputHandHolderHand(
                table.getDealerHand(),
                table.getDealer().getName(),
                hideDownCard);
        table.getPlayers()
                .forEach(player -> player.getHands()
                    .forEach(hand -> outputHandHolderHand(hand.getCards(), player.getName())));
        outputSeparator();
    }

    @Override
    public void outputActions(final Set<HandAction> possibleActions) {
        output(format(
                "Possible actions: %s",
                possibleActions.stream()
                        .map(handAction -> format(
                                "%s (%s)",
                                handAction.getDisplayName(),
                                handAction.getInputText()))
                        .collect(joining(", "))));
    }

    @Override
    public void outputHandHolderHand(final List<Card> hand, final String name) {
        outputHandHolderHand(hand, name, false);
    }

    private void outputSeparator() {
        output("################################################################################");
    }

    private void outputHandHolderHand(
            final List<Card> hand,
            final String name,
            final boolean hideSecondCard) {
        final List<Card> displayHand = new ArrayList<>(hand);
        if (hideSecondCard) {
            displayHand.remove(1);
        }

        output(format(
                "%s has %s: %s",
                name,
                getHandDisplayValue(displayHand),
                displayHand));
    }
}
