package com.mollberg.javablackjack.game.hand.holder.player.input;

import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.table.Table;

public interface PlayerInputService {
    boolean continueGame();

    HandAction getAction(Hand hand, Player player, Table table);

    int getWager(Player player);
}
