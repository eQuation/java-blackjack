package com.mollberg.javablackjack.game.hand.holder.player.output;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.table.Table;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@Profile("book")
public class NullPlayerOutputService implements PlayerOutputService {
    @Override
    public void output(String output) {
    }

    @Override
    public void output(String output, boolean newLine) {
    }

    @Override
    public void clearOutput() {
    }

    @Override
    public void outputMoney(Player player) {
    }

    @Override
    public void outputTableInfo(Table table, boolean hideDownCard) {
    }

    @Override
    public void outputActions(Set<HandAction> possibleActions) {
    }

    @Override
    public void outputHandHolderHand(List<Card> hand, String name) {
    }
}
