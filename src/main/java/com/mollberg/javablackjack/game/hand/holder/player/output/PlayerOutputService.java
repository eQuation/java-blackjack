package com.mollberg.javablackjack.game.hand.holder.player.output;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.table.Table;

import java.util.List;
import java.util.Set;

public interface PlayerOutputService {
    void output(String output);

    void output(String output, boolean newLine);

    void clearOutput();

    void outputMoney(Player player);

    void outputTableInfo(Table table, boolean hideDownCard);

    void outputActions(final Set<HandAction> possibleActions);

    void outputHandHolderHand(List<Card> hand, String name);
}
