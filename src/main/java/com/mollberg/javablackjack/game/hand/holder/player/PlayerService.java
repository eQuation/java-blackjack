package com.mollberg.javablackjack.game.hand.holder.player;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.input.PlayerInputService;
import com.mollberg.javablackjack.game.hand.holder.player.output.PlayerOutputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.mollberg.javablackjack.util.HandUtils.isBlackJack;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.concat;

@Service
public class PlayerService {

    private final PlayerInputService playerInputService;

    private final PlayerOutputService playerOutputService;

    private final double blackjackPayoutNumerator = 3;
    private final double blackjackPayoutDenominator = 2;
    private final double blackjackModifier = (blackjackPayoutNumerator + blackjackPayoutDenominator) /
            (2 * blackjackPayoutDenominator);

    @Autowired
    public PlayerService(
            final PlayerInputService playerInputService,
            final PlayerOutputService playerOutputService) {
        this.playerInputService = playerInputService;
        this.playerOutputService = playerOutputService;
    }

    public List<Player> createPlayers(final int count) {
        return IntStream.of(count)
                .mapToObj(this::createPlayer)
                .collect(toList());
    }

    public void placeWager(final Player player, final Hand hand) {
        final int wager = playerInputService.getWager(player);
        hand.setWager(wager);
        player.setMoney(player.getMoney() - wager);
    }

    public boolean isBankrupt(final Player player) {
        return player.getMoney() <= 0;
    }

    private Player createPlayer(final int playerNumber) {
        Player player = Player.builder()
                .money(1000)
                .build();
        player.setName(format("Player %d", playerNumber));

        return player;
    }

    public void pay(final Player player, final Hand hand) {

        playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());

        double modifier = 2;
        if (isBlackJack(hand.getCards()) && !hand.isFromSplit()) {
            modifier *= blackjackModifier;
        }

        if (hand.didDouble()) {
            modifier *= 2;
        }

        playerOutputService.output("Player wins!");
        player.setMoney(player.getMoney() + toIntExact(round(modifier * hand.getWager())));
    }

    public void push(final Player player, final Hand hand) {
        playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());
        playerOutputService.output("Push.");
        player.setMoney(player.getMoney() + hand.getWager());
    }

    public void take(final Player player, final Hand hand) {
        playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());
        playerOutputService.output("Player loses.");
    }

    public void handleDouble(final Player player, final Hand hand) {
        hand.didDouble(true);
        player.setMoney(player.getMoney() - hand.getWager());
    }

    public void handleSplit(final Player player, final Hand hand) {
        final Card card = hand.getCards().remove(hand.getCards().size() - 1);
        final int handIndex = player.getHands().indexOf(hand);

        final Hand newHand = Hand.builder()
                .cards(new ArrayList<>(singletonList(card)))
                .wager(hand.getWager())
                .fromSplit(true)
                .build();

        hand.setFromSplit(true);

        final List<Hand> hands = concat(
                player.getHands().subList(0, handIndex + 1).stream(),
                Stream.of(newHand))
                .collect(toList());

        if (player.getHands().size() > handIndex + 1) {
            hands.addAll(player.getHands().subList(handIndex + 1, player.getHands().size()));
        }
        player.setHands(hands);
        player.setMoney(player.getMoney() - hand.getWager());
    }
}
