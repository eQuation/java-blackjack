package com.mollberg.javablackjack.game.hand.holder.dealer;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.hand.holder.Person;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Dealer extends Person {

    public Dealer(final String name) {
        super();
        this.setName(name);
    }

    List<Card> cards;
}
