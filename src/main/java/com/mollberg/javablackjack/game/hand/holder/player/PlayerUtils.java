package com.mollberg.javablackjack.game.hand.holder.player;

import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.util.HandUtils;
import lombok.experimental.UtilityClass;

import java.util.HashSet;
import java.util.Set;

import static com.mollberg.javablackjack.game.constant.HandAction.*;
import static com.mollberg.javablackjack.util.HandUtils.*;

@UtilityClass
public class PlayerUtils {
    public static Set<HandAction> getPossibleActions(final Player player, final Hand hand) {
        final Set<HandAction> possibleActions = new HashSet<>(Set.of(STAND));
        if (canHit(hand)) {
            possibleActions.add(HIT);
        }
        if (canDouble(player, hand)) {
            possibleActions.add(DOUBLE);
        }
        if (canSplit(player, hand)) {
            possibleActions.add(SPLIT);
        }

        return possibleActions;
    }
}
