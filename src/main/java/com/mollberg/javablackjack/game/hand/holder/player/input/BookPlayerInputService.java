package com.mollberg.javablackjack.game.hand.holder.player.input;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.strategy.BookStrategy;
import com.mollberg.javablackjack.game.table.Table;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.mollberg.javablackjack.game.constant.HandAction.DOUBLE;
import static com.mollberg.javablackjack.game.constant.HandAction.SPLIT;
import static com.mollberg.javablackjack.util.HandUtils.canDouble;
import static com.mollberg.javablackjack.util.HandUtils.canSplit;

@Service
@Profile("book")
public class BookPlayerInputService implements PlayerInputService {
    private int handsPlayed = 0;

    @Override
    public boolean continueGame() {
        handsPlayed++;
        return handsPlayed < 100000;
    }

    @Override
    public HandAction getAction(final Hand hand, final Player player, final Table table) {
        final List<Card> dealerHand = table.getDealerHand();
        final Set<HandAction> excludedHandActions = new HashSet<>();
        if (!canSplit(player, hand)) {
            excludedHandActions.add(SPLIT);
        }
        if (!canDouble(player, hand)) {
            excludedHandActions.add(DOUBLE);
        }
        return BookStrategy.getAction(player, hand, dealerHand, excludedHandActions);
    }

    @Override
    public int getWager(Player player) {
        return 10;
    }
}
