package com.mollberg.javablackjack.game.hand.holder;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public abstract class Person {

    private String name;
}
