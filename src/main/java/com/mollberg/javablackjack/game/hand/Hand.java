package com.mollberg.javablackjack.game.hand;

import com.mollberg.javablackjack.game.constant.Card;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;


@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Hand {

    List<Card> cards;

    @Builder.Default
    @Accessors(fluent = true)
    private boolean didDouble = false;

    private boolean fromSplit;

    private int wager;
}
