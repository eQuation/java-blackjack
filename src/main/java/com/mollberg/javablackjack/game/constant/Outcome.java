package com.mollberg.javablackjack.game.constant;

public enum Outcome {
    LOSE,
    PUSH,
    WIN
}
