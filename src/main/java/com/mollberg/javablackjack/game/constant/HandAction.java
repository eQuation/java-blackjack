package com.mollberg.javablackjack.game.constant;

import lombok.Getter;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

@Getter
public enum HandAction {
    DOUBLE("Double", "d"),
    HIT("Hit", "h"),
    SPLIT("Split", "p"),
    STAND("Stand", "s");

    private final String displayName;

    private final String inputText;

    HandAction(final String displayName, final String inputText) {
        this.displayName = displayName;
        this.inputText = inputText;
    }

    public static List<String> allInputTexts() {
        return stream(HandAction.values()).map(HandAction::getInputText).collect(toList());
    }

    public static HandAction fromInputText(final String inputText) {
        return stream(HandAction.values())
                .filter(handAction -> handAction.getInputText().equals(inputText))
                .findFirst()
                .orElseGet(() -> null);
    }
}
