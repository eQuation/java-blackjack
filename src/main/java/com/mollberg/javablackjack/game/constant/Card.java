package com.mollberg.javablackjack.game.constant;

import lombok.Getter;

@Getter
public enum Card {
    KING(10),
    QUEEN(10),
    JACK(10),
    TEN(10),
    NINE(9),
    EIGHT(8),
    SEVEN(7),
    SIX(6),
    FIVE(5),
    FOUR(4),
    THREE(3),
    TWO(2),
    ACE(1);

    private final int cardValue;

    Card(final int cardValue) {
        this.cardValue = cardValue;
    }
}
