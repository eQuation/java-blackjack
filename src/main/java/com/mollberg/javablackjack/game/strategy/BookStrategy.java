package com.mollberg.javablackjack.game.strategy;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import lombok.experimental.UtilityClass;

import java.util.*;

import static com.mollberg.javablackjack.util.ConfigurationReader.*;
import static com.mollberg.javablackjack.util.HandUtils.*;
import static java.lang.String.format;

@UtilityClass
public class BookStrategy {
    private final Map<Integer, Map<Integer, HandAction>> hardPlayerDealerSumToHandActionMap = getHardPlayerDealerHandActionMap();

    private final Map<Integer, Map<Integer, HandAction>> softPlayerDealerSumToHandActionMap = getSoftPlayerDealerHandActionMap();

    private final Map<Integer, Map<Integer, HandAction>> splitPlayerDealerSumToHandActionMap = getSplitPlayerDealerHandActionMap();

    public static HandAction getAction(
            final Player player,
            final Hand playerHand,
            final List<Card> dealerHand,
            final Set<HandAction> excludedHandActions) {
        final Integer playerSum = getHandValue(playerHand.getCards());
        final Integer dealerUpCard = dealerHand.get(0).getCardValue();

        final List<Map<Integer, Map<Integer, HandAction>>> orderedActionMaps = new ArrayList<>();
        if (canSplit(player, playerHand)) {
            orderedActionMaps.add(splitPlayerDealerSumToHandActionMap);
        }
        if (isSoft(playerHand.getCards())) {
            orderedActionMaps.add(softPlayerDealerSumToHandActionMap);
        }
        orderedActionMaps.add(hardPlayerDealerSumToHandActionMap);

        return orderedActionMaps.stream()
                .map(map -> map.get(playerSum))
                .filter(Objects::nonNull)
                .map(map -> map.get(dealerUpCard))
                .filter(Objects::nonNull)
                .filter(handAction -> !excludedHandActions.contains(handAction))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(format(
                        "Unable to find HandAction for player hand %s player sum %d dealer up card %d",
                        playerHand,
                        playerSum,
                        dealerUpCard)));
    }
}
