package com.mollberg.javablackjack.game.table;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.dealer.DealerService;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.hand.holder.player.PlayerService;
import com.mollberg.javablackjack.game.hand.holder.player.input.PlayerInputService;
import com.mollberg.javablackjack.game.hand.holder.player.output.PlayerOutputService;
import com.mollberg.javablackjack.game.shoe.ShoeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static com.mollberg.javablackjack.game.constant.HandAction.HIT;
import static com.mollberg.javablackjack.game.constant.HandAction.STAND;
import static com.mollberg.javablackjack.util.HandUtils.*;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class TableService {

    private final DealerService dealerService;

    private final PlayerOutputService playerOutputService;

    private final PlayerService playerService;

    private final ShoeService shoeService;

    private final PlayerInputService playerInputService;

    private Table table;

    @Autowired
    public TableService(
            final DealerService dealerService,
            final PlayerOutputService playerOutputService,
            final PlayerService playerService,
            final ShoeService shoeService,
            final PlayerInputService playerInputService) {
        this.dealerService = dealerService;
        this.playerOutputService = playerOutputService;
        this.playerService = playerService;
        this.playerInputService = playerInputService;
        this.shoeService = shoeService;
    }

    public void initialize() {
        table = Table.builder()
                .players(playerService.createPlayers(1))
                .build();
        startTable();
    }

    private Card dealCard() {
        return shoeService.dealCard(table.getShoe());
    }

    private List<Card> newCardList() {
        return new ArrayList<>(singletonList(dealCard()));
    }

    private List<Hand> getPlayerHands() {
        return table.getPlayers().stream()
                .map(Player::getHands)
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private void dealHand() {
        table.getDealer().setCards(newCardList());
        getPlayerHands().forEach(hand -> hand.setCards(newCardList()));
        Stream.concat(
                Stream.of(table.getDealerHand()),
                getPlayerHands().stream().map(Hand::getCards))
                .forEach(cardList -> cardList.add(dealCard()));
    }

    private void eliminatePlayer(final Player player) {
        playerOutputService.output("Player eliminated.");
        table.getPlayers().remove(player);
    }

    private void eliminatePlayers() {
        table.getPlayers().stream()
                .filter(playerService::isBankrupt)
                .collect(toList())
                .forEach(this::eliminatePlayer);
    }

    private void newShoe() {
        table.setShoe(shoeService.newShoe());
        while (stillPlayable()) {
            playRound();
        }
    }

    private void placeWagers() {
        table.getPlayers().stream()
                .map(player -> player.setHands(new ArrayList<>(singletonList(new Hand()))))
                .forEach(player -> player.getHands()
                        .forEach(hand -> playerService.placeWager(player, hand)));
    }

    private void playDealerHand() {
        if (dealerService.shouldPlay(getPlayerHands())) {
            final List<Card> cardList = table.getDealerHand();
            HandAction action = HIT;
            while (notBusted(cardList) && !STAND.equals(action)) {
                action = dealerService.getAction(cardList);
                handleDealerAction(action);
            }
        }
    }

    private void playPlayerHands() {
        playerOutputService.outputTableInfo(table, true);
        table.getPlayers().forEach(this::playPlayerHands);
    }

    private void playPlayerHands(final Player player) {
        int i = 0;
        while (i < player.getHands().size()) {
            Hand hand = player.getHands().get(i);
            playPlayerHand(player, hand);
            i++;
        }

    }

    private void playPlayerHand(final Player player, final Hand hand) {
        if (isBlackJack(hand.getCards()) && !hand.isFromSplit()) {
            playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());
            playerOutputService.output("Blackjack!");
            return;
        }

        HandAction action = HIT;
        while (
                notBusted(hand.getCards()) &&
                        !STAND.equals(action) &&
                        !hand.didDouble()) {
            if (hand.getCards().size() == 1) {
                hand.getCards().add(dealCard());
            }
            action = playerInputService.getAction(
                    hand,
                    player,
                    table);
            handlePlayerAction(
                    action,
                    hand,
                    player);
        }

        playerOutputService.outputHandHolderHand(hand.getCards(), player.getName());
    }

    private void handleDealerAction(final HandAction action) {
        handlePlayerAction(action, Hand.builder().cards(table.getDealerHand()).build(), null);
    }

    private void handlePlayerAction(
            final HandAction action,
            final Hand hand,
            final Player player) {
        switch (action) {
            case DOUBLE:
                if (canDouble(player, hand)) {
                    playerService.handleDouble(player, hand);
                    hand.getCards().add(dealCard());
                }
                break;
            case HIT:
                hand.getCards().add(dealCard());
                break;
            case SPLIT:
                if (canSplit(player, hand)) {
                    playerService.handleSplit(player, hand);
                }
                break;
            case STAND:
                break;
        }
    }

    private void playRound() {
        placeWagers();
        dealHand();
        if (shouldPlayHands()) {
            playPlayerHands();
            playDealerHand();
        }
        resolvePayouts();
        eliminatePlayers();
        cleanUp();
    }

    private boolean shouldPlayHands() {
        return !isBlackJack(table.getDealerHand());
    }

    private void cleanUp() {
        table.getPlayers().forEach(player -> player.setHands(null));
        table.getDealer().setCards(null);
        if (playerInputService.continueGame()) {
            playerOutputService.clearOutput();
        } else {
            System.exit(0);
        }
    }

    private void resolvePayouts() {
        playerOutputService.outputTableInfo(table, false);
        final int dealerSum = getHardHandValue(table.getDealerHand());
        table.getPlayers().forEach(player -> resolvePayout(player, dealerSum));
    }

    private void resolvePayout(final Player player, final int dealerSum) {
        player.getHands().forEach(hand -> {
            switch (getHandOutcome(hand.getCards(), dealerSum)) {
                case LOSE:
                    playerService.take(player, hand);
                    break;
                case PUSH:
                    playerService.push(player, hand);
                    break;
                case WIN:
                    playerService.pay(player, hand);
                    break;
            }
        });
    }

    private void startTable() {
        boolean continueGame = true;
        while (continueGame) {
            newShoe();
            continueGame = playerInputService.continueGame();
        }
    }

    private boolean stillPlayable() {
        return shoeService.stillPlayable(table.getShoe()) &&
                !table.getPlayers().isEmpty();
    }
}
