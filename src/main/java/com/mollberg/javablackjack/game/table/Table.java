package com.mollberg.javablackjack.game.table;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.hand.holder.dealer.Dealer;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import com.mollberg.javablackjack.game.shoe.Shoe;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Table {

    private final Dealer dealer = new Dealer("Dealer");

    private List<Player> players;

    private Shoe shoe;

    public List<Card> getDealerHand() {
        return dealer.getCards();
    }
}
