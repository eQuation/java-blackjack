package com.mollberg.javablackjack.util;

import com.mollberg.javablackjack.game.constant.HandAction;
import lombok.experimental.UtilityClass;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static java.util.Objects.nonNull;

@UtilityClass
public class ConfigurationReader {
    public static Map<Integer, Map<Integer, HandAction>> getHardPlayerDealerHandActionMap() {
        final String fileReference = "strategy/book-strategy-hard.csv";
        return playerDealerHandActionMap(fileReference);
    }

    public static Map<Integer, Map<Integer, HandAction>> getSoftPlayerDealerHandActionMap() {
        final String fileReference = "strategy/book-strategy-soft.csv";
        return playerDealerHandActionMap(fileReference);
    }

    public static Map<Integer, Map<Integer, HandAction>> getSplitPlayerDealerHandActionMap() {
        final String fileReference = "strategy/book-strategy-split.csv";
        return playerDealerHandActionMap(fileReference);
    }

    private Map<Integer, Map<Integer, HandAction>> playerDealerHandActionMap(final String fileReference) {
        return parseFile(
                fileReference,
                new HashMap<>(),
                (Parser<Map<Integer, Map<Integer, HandAction>>>) (values, returnObject) -> {
                    final Integer playerValue = Integer.valueOf(values[0]);
                    final Integer dealerValue = Integer.valueOf(values[1]);
                    final HandAction action = HandAction.valueOf(values[2]);

                    if (!returnObject.containsKey(playerValue)) {
                        returnObject.put(playerValue, new HashMap<>());
                    }

                    returnObject.get(playerValue).put(dealerValue, action);
                }
        );
    }

    private <T> T parseFile(final String fileReference, final T returnObject, final Parser<T> parser) {
        final Resource file = new ClassPathResource(fileReference);
        String line;

        try (final BufferedReader br = new BufferedReader(new InputStreamReader((file.getInputStream())))) {
            while (nonNull((line = br.readLine()))) {
                if (line.isEmpty() || line.trim().startsWith("//")) {
                    continue;
                }
                parser.parse(line.split(","), returnObject);
            }

            return returnObject;
        } catch (IOException e) {
            throw new RuntimeException(format("Unable to build configuration map for %s", fileReference), e);
        }
    }

    private interface Parser<T> {
        void parse(final String[] values, T returnObject);
    }
}
