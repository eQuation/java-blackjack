package com.mollberg.javablackjack.util;

import lombok.experimental.UtilityClass;

import static java.lang.Integer.parseInt;

@UtilityClass
public class IntegerUtils {

    public static boolean isInt(final String value) {
        try {
            parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
