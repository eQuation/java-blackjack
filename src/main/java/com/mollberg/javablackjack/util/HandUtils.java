package com.mollberg.javablackjack.util;

import com.mollberg.javablackjack.game.constant.Card;
import com.mollberg.javablackjack.game.constant.HandAction;
import com.mollberg.javablackjack.game.constant.Outcome;
import com.mollberg.javablackjack.game.hand.Hand;
import com.mollberg.javablackjack.game.hand.holder.player.Player;
import lombok.experimental.UtilityClass;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.mollberg.javablackjack.game.constant.Card.ACE;
import static com.mollberg.javablackjack.game.constant.HandAction.*;
import static com.mollberg.javablackjack.game.constant.Outcome.*;
import static java.lang.String.format;

@UtilityClass
public class HandUtils {

    public static boolean isBusted(final List<Card> hand) {
        return getHardHandValue(hand) > 21;
    }

    public static boolean notBusted(final List<Card> hand) {
        return !isBusted(hand);
    }

    public static String getHandDisplayValue(final List<Card> hand) {
        final int sum = getHandValue(hand);

        if (!isSoft(hand)) {
            if (sum > 21) {
                return format("%d, bust", sum);
            } else {
                return String.valueOf(sum);
            }
        }

        return format("soft %d", sum + 10);
    }

    public static boolean playerWon(final int handValue, final int dealerSum) {
        if (handValue > 21) {
            return false;
        }
        if (dealerSum > 21) {
            return true;
        }
        return handValue > dealerSum;
    }

    public static Outcome getHandOutcome(final List<Card> playerCards, final int dealerSum) {
        final int handValue = getHardHandValue(playerCards);
        if (playerWon(handValue, dealerSum)) {
            return WIN;
        } else if (handValue == dealerSum) {
            return PUSH;
        } else {
            return LOSE;
        }
    }

    public static int getHandValue(final List<Card> hand) {
        return hand.stream()
                .mapToInt(Card::getCardValue)
                .sum();
    }

    public static boolean canDouble(final Player player, final Hand hand) {
        return hand.getWager() <= player.getMoney() &&
                hand.getCards().size() == 2 &&
                getHardHandValue(hand.getCards()) < 21;
    }

    public static boolean canSplit(final Player player, final Hand hand) {
        return player.getMoney() >= hand.getWager() &&
                hand.getCards().size() == 2 &&
                hand.getCards().get(0).getCardValue() == hand.getCards().get(1).getCardValue();
    }

    public static boolean canHit(final Hand hand) {
        return getHardHandValue(hand.getCards()) < 21;
    }

    public static int getHardHandValue(final List<Card> hand) {
        final int softValue = getHandValue(hand);
        return isSoft(hand) ? softValue + 10 : softValue;
    }

    public static boolean isSoft(final List<Card> hand) {
        return (hand.contains(ACE) && getHandValue(hand) < 12);
    }

    public static boolean isBlackJack(final List<Card> cards) {
        return getHardHandValue(cards) == 21 && cards.size() == 2;
    }
}
